package test;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

/**
 *
 * @author Administrator
 */
public class Test {
    
    JFrame jf = new JFrame();
    JPanel jp = new JPanel();
    /**
     * 显示题目
     */
    JLabel title = new JLabel(); 
    /**
     * 显示结果正确与否
     */
    JLabel result = new JLabel(); 
    /**
     * 显示排行榜
     */
    JLabel list = new JLabel();
    JButton submit = new JButton("提交");
    JButton next = new JButton("next");
    JButton ranking = new JButton("排行榜");
    /**
     * 输入答案
     */
    JTextField answer = new JTextField(); 
    
public Test(){

    Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
    int width = (int)screensize.getWidth();
    int height = (int)screensize.getHeight();	
    int w=width/2-150;
    int h=height/2-150;
    jf.setBounds(w, h, 300, 400);
    jf.add(jp);
    //取消布局管理器
    jp.setLayout(null);
    jp.add(title);
    
    //调整位置，大小，前两个坐标，后两个宽度和高度
    title.setBounds(42,18,100,10); 
    jp.add(answer);
    answer.setBounds(140,10,40,20);
    jp.add(submit);
    submit.setBounds(40,50,80,40);
    jp.add(next);
    next.setBounds(140,50,80,40);
    jp.add(result);
    result.setBounds(40,100,200,50);
    jp.add(ranking);
    ranking.setBounds(90,160,80,40);
    jp.add(list);
    list.setBounds(40,260,200,50);
    //设置窗体可见
    jf.setVisible(true);
    //结束窗口所在的应用程序
    jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
}
public void produce() {
	int num3,num4,temp1;
	Data test = new Data();
	//Msth.random()随机取大于0.0小于1.0的数
	num3 = new Random().nextInt(100);
	num4 = new Random().nextInt(100);
	temp1 = new Random().nextInt(100);
    test.setNum1(num3);
    test.setNum2(num4);
    test.setSum(0);
    //随机生成加减
    test.setTemp(temp1 % 2);
    if ((test.getTemp()) == 0) {
        test.setCount((test.getCount())+1);
        title.setText("第"+(test.getCount())+"题："+test.getNum1() + "+" + test.getNum2() + "=");
        test.setSum((test.getNum1())+(test.getNum2()));
    }
    else {
    	test.setCount((test.getCount())+1);
    	title.setText("第"+(test.getCount())+"题："+test.getNum1() + "-" + test.getNum2() + "=");
        test.setSum((test.getNum1())-(test.getNum2()));
    }
submit.addActionListener((ActionEvent e) -> {
    int s = Integer.parseInt(answer.getText());
    if(s==test.getSum()){ 
        result.setText("恭喜，回答正确！");   
        test.setScore((test.getScore())+10);
    }else{
        result.setText("抱歉，回答错误。"+"正确答案：" + test.getSum());
    }
});
next.addActionListener((ActionEvent e) -> {
	String a = null;
	FileAccess file3 = new FileAccess();
    a = file3.fileReader();
    test.setMost(Integer.parseInt(a));
    //判断分数是否最高
    if (test.getScore()>test.getMost()){
    	test.setMost(test.getScore());
    	FileAccess file1 = new FileAccess();
        file1.fileWriter(test.getMost());
    }
    int num1,num2,temp;
    int y = 10;
    if(test.getCount()==y){
        JOptionPane.showMessageDialog(jf, "您的得分是："+test.getScore());
        test.setCount(0);
        test.setScore(0);
    }
    num1 = new Random().nextInt(100);
    num2 = new Random().nextInt(100);
    temp = new Random().nextInt(100);
    test.setNum1(num1);
    test.setNum2(num2);
    test.setSum(0);
    test.setTemp(temp%2);
    if ((test.getTemp()) == 0) {
        test.setCount((test.getCount())+1);
        title.setText("第"+(test.getCount())+"题："+test.getNum1() + "+" + test.getNum2() + "=");
        test.setSum((test.getNum1())+(test.getNum2()));
    }
    else {
    	test.setCount((test.getCount())+1);
    	title.setText("第"+(test.getCount())+"题："+test.getNum1() + "-" + test.getNum2() + "=");
        test.setSum((test.getNum1())-(test.getNum2()));
    }
  
});
ranking.addActionListener((ActionEvent e) -> {
	FileAccess file = new FileAccess();
	list.setText("最高分为："+file.fileReader());
});


}




public static void main(String args[]) {

Test a = new Test();

a.produce();
}

}

